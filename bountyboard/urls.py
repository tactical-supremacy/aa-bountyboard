from django.urls import path

from . import views

app_name: str = "bountyboard"

urlpatterns = [
    path("", views.index, name="index"),
    path(
        'bountyprogram/<int:program_id>/',
        views.bountyprogram, name='bountyprogram'),
    path(
        'bountyprogram/<int:program_id>/request/',
        views.bounty_request, name='bounty_request'),
]
