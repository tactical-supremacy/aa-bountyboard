from decimal import Decimal

from eveuniverse.models import (
    EveConstellation, EveGroup, EveRegion, EveSolarSystem, EveType,
)

from django.contrib.auth.models import User
from django.core.exceptions import ValidationError
# from .tasks import populate_price_from_zkill
from django.core.validators import MaxValueValidator, MinValueValidator
from django.db import models
from django.db.models import Sum
from django.utils.translation import gettext_lazy as _

from allianceauth.eveonline.models import EveAllianceInfo, EveCorporationInfo
from allianceauth.services.hooks import get_extension_logger

from .providers import (
    get_killmails_killmail_id_killmail_hash, zkill_get_killmail,
)

logger = get_extension_logger(__name__)


class General(models.Model):
    """Meta model for app permissions"""

    class Meta:
        managed = False
        default_permissions = ()
        permissions = (
            ("basic_access", "Can access the Bounty Board App"),
            ("bounty_request", "Can request a payout for a Killmail in a Bounty Program"),
            ("bounty_approve", "Can mark a request as Approved"),
            ("bounty_pay", "Can mark a request as Paid"),
            ("manage_bountyprogram", "Can manage bounty programs"),
        )


class KillmailAttacker(models.Model):
    alliance_id = models.IntegerField(
        _("Alliance ID"),
        blank=True,
        null=True)
    character_id = models.IntegerField(
        _("Character ID"),
        blank=True,
        null=True)
    corporation_id = models.IntegerField(
        _("Corporation ID"),
        blank=True,
        null=True)
    faction_id = models.IntegerField(
        _("Faction ID"),
        blank=True,
        null=True)
    damage_done = models.IntegerField(
        _("Damage Done"))

    class Meta:
        """
        Meta definitions
        """
        default_permissions = ()


class Killmail(models.Model):

    killmail_hash = models.CharField(
        _("Killmail Hash"),
        max_length=50)
    killmail_time = models.DateTimeField(
        _("Killmail Date/Time"),
        auto_now=False,
        auto_now_add=False)
    solar_system = models.ForeignKey(
        EveSolarSystem,
        verbose_name=_("Solar System"),
        on_delete=models.CASCADE)
    alliance_id = models.IntegerField(
        _("Alliance ID"),
        blank=True,
        null=True)
    character_id = models.IntegerField(
        _("Character ID"),
        blank=True,
        null=True)
    corporation_id = models.IntegerField(
        _("Corporation ID"),
        blank=True,
        null=True)
    faction_id = models.IntegerField(
        _("Faction ID"),
        blank=True,
        null=True)
    damage_taken = models.IntegerField(
        _("Damage Taken"))
    ship = models.ForeignKey(
        EveType,
        verbose_name=_("Ship Hull"),
        on_delete=models.CASCADE)
    attackers = models.ManyToManyField(
        KillmailAttacker,
        verbose_name=_("Attackers"))
    zkill_fitted_value = models.DecimalField(
        _("zKill Fitted Value"),
        decimal_places=2,
        max_digits=15,
        default=Decimal(0))
    zkill_total_value = models.DecimalField(
        _("zKill Total Value"),
        decimal_places=2,
        max_digits=15,
        default=Decimal(0))

    @classmethod
    def create_from_esi(cls, hash: str, id: int) -> models.Model:
        "Create a killmail from ESI"
        esi = get_killmails_killmail_id_killmail_hash(hash=hash, id=id)
        killmail = Killmail()
        killmail.id = id
        killmail.killmail_hash = hash
        killmail.killmail_time = esi["killmail_time"]
        killmail.solar_system, created = EveSolarSystem.objects.get_or_create_esi(
            id=esi["solar_system_id"])
        killmail.alliance_id = esi["victim"]["alliance_id"]
        killmail.corporation_id = esi["victim"]["corporation_id"]
        killmail.character_id = esi["victim"]["character_id"]
        killmail.faction_id = esi["victim"]["faction_id"]
        killmail.damage_taken = esi["victim"]["damage_taken"]
        killmail.ship, created = EveType.objects.get_or_create_esi(
            id=esi["victim"]["ship_type_id"])
        for attacker in esi["attackers"]:
            killmailattacker = KillmailAttacker.objects.create(
                alliance_id=attacker["alliance_id"],
                character_id=attacker["character_id"],
                corporation_id=attacker["corporation_id"],
                faction_id=attacker["faction_id"],
                damage_done=attacker["damage_done"],
            )
            killmail.attackers.add(killmailattacker)
        killmail.save()
        # populate_price_from_zkill.delay(killmail_id = id)
        return killmail

    @classmethod
    def create_from_zkill(cls, id: int) -> models.Model:
        "Parse from zkill, then create from ESI"
        zkill = zkill_get_killmail(kill_id=id)
        killmail = Killmail.create_from_esi(id=id, hash=zkill["zkb"]["hash"])
        killmail.zkill_fitted_value = zkill["zkb"]["fittedValue"]
        killmail.zkill_total_value = zkill["zkb"]["totalValue"]
        killmail.save()
        return killmail

    class Meta:
        """
        Meta definitions
        """
        default_permissions = ()


class BountyProgram(models.Model):
    """A posted bounty, which will pay out bounties based on filters"""

    name = models.TextField(_("Bounty Program"))
    start_date = models.DateTimeField(
        _("Start Date"),
        auto_now=False,
        auto_now_add=False)
    end_date = models.DateTimeField(
        _("End Date"),
        auto_now=False,
        auto_now_add=False)
    # Modifiers
    PAYOUT_METHOD_CHOICES = [
        ('zkillboard_fitted', _('zKill Fit+Hull Value')),
        ('zkillboard_total', _('zKill Total Value')),
        # ('jita_hull_buy', _('Jita (Buy) Hull Value')),
        # ('jita_hull_sell', _('Jita (Sell) Hull Value')),
        ('fixed', _('Fixed Payout')),
    ]
    payout_pricing_method = models.CharField(
        _("Payout Pricing Method"),
        max_length=20,
        choices=PAYOUT_METHOD_CHOICES,
        help_text="The Payout Pricing Method to use",
        default="killboard_fit")
    payout_modifier = models.DecimalField(
        _("zKill/Jita Payout Modifier"),
        help_text="% Modifier for zKill and Jita pricing methods",
        max_digits=4,
        decimal_places=0,
        default=Decimal(100),
        validators=[MinValueValidator(0), MaxValueValidator(1000)])
    fixed_payout_isk = models.DecimalField(
        _("Fixed/Minimum Payout (ISK)"),
        decimal_places=2,
        max_digits=15,
        default=Decimal(0),
        help_text="Price when used Fixed Pricing Method above, May also be used as a Minimum payout value when using zkill/jita",)

    PAYOUT_SHARING_CHOICES = [
        ('final_blow', _('Final Blow')),
        ('top_damage', _('Top DPS')),
        # ('equal_share', _('Equal Share')),
        # ('participation', _('Ratio Share on Damage %')),
    ]
    payout_sharing_method = models.CharField(
        _("Payout Sharing Method"),
        max_length=20,
        choices=PAYOUT_SHARING_CHOICES,
        help_text="The Payout Sharing Method to use",
        default="final_blow")
    payout_once_per_user = models.BooleanField(
        _("Only Pay Bounty Shares once per User"),
        default=False)
    # Filters
    # Type
    types = models.ManyToManyField(
        EveType,
        verbose_name=_("Ship"),
        blank=True)
    groups = models.ManyToManyField(
        EveGroup,
        verbose_name=_("Ship Class"),
        blank=True)
    # Location
    solar_systems = models.ManyToManyField(
        EveSolarSystem,
        verbose_name=_("Solar System"),
        blank=True)
    constellations = models.ManyToManyField(
        EveConstellation,
        verbose_name=_("Constellations"),
        blank=True)
    regions = models.ManyToManyField(
        EveRegion,
        verbose_name=_("Region"),
        blank=True)
    # Targets
    corporations = models.ManyToManyField(
        EveCorporationInfo,
        verbose_name=_("Corporations"),
        blank=True)
    alliances = models.ManyToManyField(
        EveAllianceInfo,
        verbose_name=_("Alliances"),
        blank=True)

    class Meta:
        """
        Meta definitions
        """
        default_permissions = ()

    def __str__(self):
        return f'{self.name}'

    @property
    def all_ships(self):
        return self.types.all() | EveType.objects.filter(eve_group__in=self.groups.all(), published=1)

    @property
    def sum_payouts(self) -> Decimal:
        return self.bountyrequest_set.aggregate(Sum('payout'))['payout__sum']

    def calculate_payout(self, killmail: Killmail, user: User) -> Decimal:
        if self.payout_pricing_method == "zkillboard_fitted":
            payout = killmail.zkill_fitted_value
        elif self.payout_pricing_method == "zkillboard_total":
            payout = killmail.zkill_total_value
        elif self.payout_pricing_method == "fixed":
            payout = self.fixed_payout_isk
        else:
            payout = 0

        # oh shit i have to work out if someone has multiple toons, then run this logic over
        # if cls.payout_sharing_method == "equal_share":
        #     payout = payout / killmail.attackers.count()
        # elif cls.payout_sharing_method == "participation":
        #     payout = payout * (killmail.attackers.get(user.profile.main_character).damage_done / killmail.damage_taken)

        return Decimal(payout) * (self.payout_modifier / 100)


class BountyRequest(models.Model):

    bounty_program = models.ForeignKey(
        BountyProgram,
        verbose_name=_("Bounty Program"),
        on_delete=models.CASCADE)
    killmail = models.ForeignKey(
        Killmail,
        verbose_name=_("Killmail"),
        on_delete=models.CASCADE)
    payout = models.DecimalField(
        _("Bounty Payout"),
        decimal_places=2,
        max_digits=15,
        default=Decimal(0))
    user = models.ForeignKey(
        User,
        verbose_name=_("User"),
        on_delete=models.CASCADE)
    created = models.DateTimeField(
        _("Created"),
        auto_now=False,
        auto_now_add=True)
    approved = models.DateTimeField(
        _("Approved"),
        auto_now=False,
        auto_now_add=False,
        blank=True,
        null=True)
    paid = models.DateTimeField(
        _("Paid"),
        auto_now=False,
        auto_now_add=False,
        blank=True,
        null=True)

    class Meta:
        """
        Meta definitions
        """
        default_permissions = ()

    def __str__(self):
        return f'{self.bounty_program.name} {self.user.username}'

    @classmethod
    def bounty_request(cls, killmail: Killmail, user: User, bounty_program: BountyProgram) -> bool:
        "Create a Bounty Request"

        if bounty_program.start_date > killmail.killmail_time:
            raise ValidationError(
                _('Killmail is from before this Bounty Program'),
                code='invalid_time')

        if bounty_program.end_date < killmail.killmail_time:
            raise ValidationError(
                _('Killmail is from after this Bounty Program'),
                code='invalid_time')

        if bounty_program.corporations.count() == 0 and bounty_program.alliances.count() == 0:
            pass
        elif bounty_program.corporations.count() != 0 and killmail.corporation_id in bounty_program.corporations.values_list('corporation_id', flat=True):
            pass
        elif bounty_program.alliances.count() != 0 and killmail.alliance_id in bounty_program.alliances.values_list('alliance_id', flat=True):
            pass
        else:
            raise ValidationError(
                _('Invalid Target Corporation or Alliance'),
                code='invalid_affiliation')

        if bounty_program.types.count() == 0 and bounty_program.groups.count() == 0:
            pass
        if bounty_program.types.count() != 0 and killmail.ship in bounty_program.types.all():
            pass
        elif bounty_program.groups.count() != 0 and killmail.ship.eve_group in bounty_program.groups.all():
            pass
        else:
            raise ValidationError(
                _('Invalid Target Ship Type or Class'),
                code='invalid_type')

        if bounty_program.solar_systems.count() == 0 and bounty_program.constellations.count() == 0 and bounty_program.regions.count() == 0:
            pass
        if bounty_program.solar_systems.count() != 0 and killmail.solar_system in bounty_program.solar_systems.all():
            pass
        elif bounty_program.constellations.count() != 0 and killmail.solar_system.eve_constellation in bounty_program.constellations.all():
            pass
        elif bounty_program.regions.count() != 0 and killmail.solar_system.eve_constellation.eve_region in bounty_program.regions.all():
            pass
        else:
            raise ValidationError(
                _('Invalid Target Location'),
                code='invalid_area')

        BountyRequest.objects.create(
            bounty_program=bounty_program,
            killmail=killmail,
            user=user,
            payout=bounty_program.calculate_payout(killmail, user)
        )
