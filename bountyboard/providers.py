from typing import Dict, Tuple

import requests

from allianceauth import __version__ as aa__version__
from allianceauth.services.hooks import get_extension_logger
from esi.clients import EsiClientProvider

from . import __version__ as bb__version__

logger = get_extension_logger(__name__)

APP_INFO_TEXT = f"allianceauth v{aa__version__} & aa-bounty-board v{bb__version__}"
"""
Swagger spec operations:
get_killmails_killmail_id_killmail_hash
"""

esi = EsiClientProvider(app_info_text=APP_INFO_TEXT)


def get_killmails_killmail_id_killmail_hash(hash: str, id: int) -> Dict:
    result = esi.client.Killmails.get_killmails_killmail_id_killmail_hash(
        killmail_hash=hash,
        killmail_id=id
    ).results()
    return result


def zkill_get_killmail(kill_id) -> Tuple[int, str]:
    url = ("https://zkillboard.com/api/killID/%s/" % kill_id)
    headers = {
        'User-Agent': APP_INFO_TEXT,
        'Content-Type': 'application/json',
        'Accept-Encoding': 'gzip'
    }
    r = requests.get(url, headers=headers)
    result = r.json()[0]
    if result:
        return result
    else:
        raise ValueError("Invalid Kill ID")
