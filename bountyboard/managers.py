from django.db import models


class BountyRequestManager(models.Manager):
    def get_queryset(self):
        return super().get_queryset().filter()
