��            )   �      �     �  	   �     �     �     �     �     �               "     +  
   C     N     [  :   v  &   �     �  !   �                     6     =  
   B     M  
   Z     e     l     t  �  �     I  	   e  	   o     y     �     �     �     �     �     �     �  
   
          #  A   ?  '   �     �  )   �     �     �  !   �          '     0     C     T  	   c     m     z                                           	                                 
                                                             Active Bounty Programs Alliances Approved Bounty Board Bounty Board - Request Bounty Program Constellations Corporations Created End Date Expired Bounty Programs Final Blow Fixed Payout Fixed/Minimum Payout (ISK) Invalid Link. Please use zkillboard.com or kb.evetools.org Invalid Target Corporation or Alliance Invalid Target Location Invalid Target Ship Type or Class Killmail Paid Payout Sharing Method Region Ship Ship Class Solar System Start Date Submit Top DPS zKill Total Value Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2023-10-08 10:23+0000
Last-Translator: Joel Falknau <ozirascal@gmail.com>, 2023
Language-Team: French (France) (https://app.transifex.com/alliance-auth/teams/107430/fr_FR/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: fr_FR
Plural-Forms: nplurals=3; plural=(n == 0 || n == 1) ? 0 : n != 0 && n % 1000000 == 0 ? 1 : 2;
 Programmes de primes actifs Alliances Approuvé Tableau des primes Tableau des primes - Demande Programme de primes Constellations Corporations Créé date de fin Programmes de primes expirés Coup final Paiement fixe Paiement fixe/minimum (ISK) Lien invalide. Veiller utilisez zkillboard.com ou kb.evetools.org Corporations ou Alliance cible invalide Emplacement cible invalide Type ou classe de vaisseau cible invalide Killmail Payé Méthode de partage des paiements Région Vaisseau Classe de vaisseau Système Solaire Date de début Soumettre Meilleur DPS Valeur total zKill 