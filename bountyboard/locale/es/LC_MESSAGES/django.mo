��    '      T  5   �      `     a  	   x     �     �     �     �     �     �     �     �     �  
               6   6  :   m  &   �     �  !   �     	          %      3  $   T     y     ~     �     �     �     �  
   �     �  
   �     �     �     �          %  �  @      �     
                1     R     j     y     �     �  "   �     �  	   �     �  G   �  F   3	  !   z	  "   �	      �	     �	     �	     
  '   
  +   :
     f
     m
     �
     �
     �
     �
     �
     �
     �
     �
       '        5     K           
                %                $              !          #                 "                 '         	              &                                                     Active Bounty Programs Alliances Approved Bounty Board Bounty Board - Request Bounty Program Constellations Corporations Created End Date Expired Bounty Programs Final Blow Fixed Payout Fixed/Minimum Payout (ISK) Invalid Link. Please post a direct link to a killmail. Invalid Link. Please use zkillboard.com or kb.evetools.org Invalid Target Corporation or Alliance Invalid Target Location Invalid Target Ship Type or Class Killmail Killmail Date/Time Killmail Hash Killmail Link - (ESI Preferrred) Only Pay Bounty Shares once per User Paid Payout Pricing Method Payout Sharing Method Region Request Bounty Payout Ship Ship Class Solar System Start Date Submit Top DPS zKill Fit+Hull Value zKill Total Value zKill/Jita Payout Modifier Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2023-10-08 10:23+0000
Last-Translator: Joel Falknau <ozirascal@gmail.com>, 2023
Language-Team: Spanish (https://app.transifex.com/alliance-auth/teams/107430/es/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: es
Plural-Forms: nplurals=3; plural=n == 1 ? 0 : n != 0 && n % 1000000 == 0 ? 1 : 2;
 Programas de recompensas activos Alianzas Aprobado Tabla de recompensas Tabla de recompensas - Solicitud Programa de recompensas Constelaciones Corporaciones Creado Fecha final Programas de recompensas expirados Golpe final Pago fijo Pago fijo/mínimo (ISK) Enlace no válido. Por favor, publique un enlace directo a un killmail. Enlace no válido. Por favor, utilice zkillboard.com o kb.evetools.org Corporación o alianza no válida Ubicación del objetivo no válido Tipo o clase de nave no válidos Killmail Fecha/Hora del killmail Hash de Killmail Enlace Killmail - (Preferiblemente ESI) Pagar recompensas sólo una vez por usuario Pagado Método de fijación de pagos Método de reparto de pagos Región Solicitar pago de recompensas Nave Clase de nave Sistema Solar Fecha de inicio Enviar Top DPS Valor Equipamiento+Estructura del zKill Valor total del zKill Modificador de pago zKill/Jita 