��    4      �  G   \      x     y  	   �     �     �     �     �     �     �     �     �     �                 
   $  
   /     :     G  6   b  :   �  &   �     �  !        5     >     Q      _  *   �  +   �     �  	   �  $   �                    ,     B     I     _  
   d     o     u  
   �     �     �     �     �     �     �     �     �  k    
   w	  	   �	  	   �	     �	     �	     �	     �	     �	     �	     �	     
     
     "
     5
     >
  
   J
     U
     f
  D   �
  D   �
  (        7  '   K     s     |     �     �  9   �  8   �     4     :  .   ?     n  
   t        !   �     �     �     �     �     �       
             $     *     @     H     [     {  "   �     )                
   '                    ,       4   +          1                     #   .   0   (          /                             !                        "   3          2   *   -   %   $                           	             &              Alliance ID Alliances Approved Bounty Board Bounty Payout Bounty Program Character ID Constellations Corporation ID Corporations Created Damage Done Damage Taken End Date Faction ID Final Blow Fixed Payout Fixed/Minimum Payout (ISK) Invalid Link. Please post a direct link to a killmail. Invalid Link. Please use zkillboard.com or kb.evetools.org Invalid Target Corporation or Alliance Invalid Target Location Invalid Target Ship Type or Class Killmail Killmail Date/Time Killmail Hash Killmail Link - (ESI Preferrred) Killmail is from after this Bounty Program Killmail is from before this Bounty Program Kills Locations Only Pay Bounty Shares once per User Open Paid Payout Pricing Method Payout Sharing Method Region Request Bounty Payout Ship Ship Class Ships Solar System Start Date Submit Targets Toggle navigation Top DPS width=64 height=64 zKill Fit+Hull Value zKill Total Value zKill/Jita Payout Modifier Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2023-10-08 10:23+0000
Last-Translator: Peter Pfeufer, 2024
Language-Team: German (https://app.transifex.com/alliance-auth/teams/107430/de/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: de
Plural-Forms: nplurals=2; plural=(n != 1);
 Allianz ID Allianzen Genehmigt Kopfgeld Board Kopfgeldauszahlung Kopfgeldprogramm Charakter ID Konstellationen Corporation ID Corporationen Erstellt Angerichteter Schaden Erlittener Schaden Enddatum Fraktion ID Final Blow Feste Auszahlung Feste/Mindestauszahlung (ISK) Ungültiger Link. Bitte poste einen direkten Link zu einer Killmail. Ungültiger Link. Bitte verwende zkillboard.com oder kb.evetools.org Ungültige Zielkorporation oder -allianz Ungültiger Zielort Ungültiger Zielschiffstyp oder -klasse Killmail Killmail Datum/Uhrzeit Killmail Hash Killmail Link - (ESI bevorzugt) Killmail stammt aus der Zeit nach diesem Kopfgeldprogramm Killmail stammt aus der Zeit vor diesem Kopfgeldprogramm Kills Orte Prämienanteile nur einmal pro Benutzer zahlen Offen Ausgezahlt Preismethode für die Auszahlung Methode zur Auszahlungsaufteilung Region Kopfgeldauszahlung anfordern Schiff Schiffsklasse Schiffe Sonnensystem Startdatum Absenden Ziele Navigation umschalten Top DPS width=64 height=64 zKill Fitting + Wert der Hülle zKill totaler Wert zKill/Jita Auszahlunbgsmodifikator 