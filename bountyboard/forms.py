import re

from django import forms
from django.utils.translation import gettext_lazy as _

from allianceauth.services.hooks import get_extension_logger

logger = get_extension_logger(__name__)


class BountyRequestForm(forms.Form):
    killmail_url = forms.CharField(
        label=_("Killmail Link - (ESI Preferrred)"),
        widget=forms.TextInput(
            attrs={'placeholder': 'https://esi.evetech.net/latest/killmails/1234567890/abcdefghijklmnopqrstuvwxyz/'}),
        max_length=255,
        required=True
    )

    def clean_killmail_url(self):
        data = self.cleaned_data['killmail_url']
        # Credit to allianceauth.srp for a lot of this
        # Check if it's a link from one of the accepted kill boards
        if not any(
            re.match(regex, data)
            for regex in [
                r"^http[s]?:\/\/esi\.evetech\.net\/",
                r"^http[s]?:\/\/zkillboard\.com\/",
                r"^http[s]?:\/\/kb\.evetools\.org\/",
            ]
        ):
            raise forms.ValidationError(
                _("Invalid Link. Please use zkillboard.com or kb.evetools.org")
            )

        # Check if it's an actual kill mail
        if not any(
            re.match(regex, data)
            for regex in [
                r"^http[s]?:\/\/esi\.evetech\.net\/latest\/killmails\/\d+\/.+\/",
                r"^http[s]?:\/\/zkillboard\.com\/kill\/\d+\/",
                r"^http[s]?:\/\/kb\.evetools\.org\/kill\/\d+",
            ]
        ):
            raise forms.ValidationError(
                _("Invalid Link. Please post a direct link to a killmail.")
            )

        return data
