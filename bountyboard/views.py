from urllib.parse import urlparse

from django.contrib import messages
from django.contrib.auth.decorators import login_required, permission_required
from django.core.exceptions import ValidationError
from django.core.handlers.wsgi import WSGIRequest
from django.http import HttpResponse
from django.shortcuts import redirect, render
from django.utils import timezone

from allianceauth.services.hooks import get_extension_logger

from .forms import BountyRequestForm
from .models import BountyProgram, BountyRequest, Killmail

logger = get_extension_logger(__name__)


@login_required
@permission_required("bountyboard.basic_access")
def index(request: WSGIRequest) -> HttpResponse:
    """
    Index view
    :param request:
    :return:
    """

    context = {
        "bountyprograms_active": BountyProgram.objects.filter(
            start_date__lt=timezone.now(),
            end_date__gt=timezone.now()
        ),
        "bountyprograms_expired": BountyProgram.objects.filter(
            start_date__gt=timezone.now(),
            end_date__lt=timezone.now()
        ),
    }

    return render(request, "bountyboard/index.html", context)


@login_required
@permission_required("bountyboard.basic_access")
def bountyprogram(request: WSGIRequest, program_id: int) -> HttpResponse:
    """
    A Bounty Program
    :param request:
    :return:
    """
    context = {}
    context['bountyprogram'] = BountyProgram.objects.get(id=program_id)

    return render(request, "bountyboard/bountyprogram.html", context)


@login_required
@permission_required("bountyboard.bounty_request")
def bounty_request(request: WSGIRequest, program_id: int) -> HttpResponse:
    """
    Index view
    :param request:
    :return:
    """
    context = {}
    bounty_program = BountyProgram.objects.get(id=program_id)
    context['form'] = BountyRequestForm()
    context['bountyprogram'] = bounty_program
    if request.method == 'POST':
        form = BountyRequestForm(request.POST)
        if form.is_valid():
            parsed_url = urlparse(form.cleaned_data['killmail_url'])
            if parsed_url.netloc == "zkillboard.com":
                split_parsed_url = parsed_url.path.split("/")
                killmail = Killmail.create_from_zkill(id=split_parsed_url[2])
            if parsed_url.netloc == "esi.evetech.net":
                split_parsed_url = parsed_url.path.split("/")
                killmail = Killmail.create_from_esi(
                    hash=split_parsed_url[4],
                    id=int(split_parsed_url[3]))
            try:
                BountyRequest.bounty_request(
                    killmail=killmail,
                    user=request.user,
                    bounty_program=BountyProgram.objects.get(id=program_id))
                messages.success(request, "Bounty Request Submitted")
            except ValidationError as e:
                messages.error(request, e.message)
                return render(request, "bountyboard/bounty_request.html", context)
            except Exception as e:
                logger.exception(e)
                messages.error(request, "An Unknown Error Occured")
            return redirect('bountyboard:bountyprogram', program_id)
        else:
            context['form'] = form
            return render(request, "bountyboard/bounty_request.html", context)
    else:
        form = BountyRequestForm()
        return render(request, "bountyboard/bounty_request.html", context)
