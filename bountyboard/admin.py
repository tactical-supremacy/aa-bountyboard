from eveuniverse.models import (
    EveConstellation, EveGroup, EveRegion, EveSolarSystem, EveType,
)

from django.contrib import admin

from allianceauth.services.hooks import get_extension_logger

from .models import BountyProgram, BountyRequest, Killmail

logger = get_extension_logger(__name__)


@admin.register(Killmail)
class KillmailAdmin(admin.ModelAdmin):
    list_display = ('id', 'ship', 'damage_taken')


@admin.register(BountyProgram)
class BountyProgramAdmin(admin.ModelAdmin):
    list_display = (
        'name',
        'start_date',
        'end_date',
        'payout_pricing_method',
        'payout_sharing_method')
    filter_horizontal = [
        "types",
        "groups",
        "regions",
        "constellations",
        "solar_systems",
        "corporations",
        "alliances"]

    def formfield_for_manytomany(self, db_field, request, **kwargs):
        # Return only Ships that are Published
        if db_field.name == "types":
            kwargs["queryset"] = EveType.objects.filter(
                eve_group__eve_category__id__in=[6], published=1)
        # Return only Ship Classes that are published
        if db_field.name == "groups":
            kwargs["queryset"] = EveGroup.objects.filter(
                eve_category__id__in=[6], published=1)
        # Non CCP regions, constellations and systems
        if db_field.name == "regions":
            kwargs["queryset"] = EveRegion.objects.filter(id__lt="11000000")
        if db_field.name == "constellations":
            kwargs["queryset"] = EveConstellation.objects.filter(
                id__lt="21000000")
        if db_field.name == "solar_systems":
            kwargs["queryset"] = EveSolarSystem.objects.filter(
                id__lt="32000000")
        return super().formfield_for_manytomany(db_field, request, **kwargs)


@admin.register(BountyRequest)
class BountyRequestAdmin(admin.ModelAdmin):
    list_display = ('bounty_program', 'user', 'created', 'approved', 'paid')
