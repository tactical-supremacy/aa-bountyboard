from django.apps import AppConfig

from . import __version__


class BountyBoardConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = "bountyboard"
    verbose_name = f'AA Bounty Board v{__version__}'
